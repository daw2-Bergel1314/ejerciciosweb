function bucle01() {
    var total = 0;
    var numUser;

    do {
        numUser = prompt("Introduce un numero bucle01") * 1;
        total = total + numUser;
    } while (num !== 0);

    console.log(total);
}
//bucle01();

function mediaNums() {
    var numUser = 0;
    var total = 0;
    var contador = 0;

    do {
        numUser = prompt("Introduce un numero mediaNums") * 1;
        total = total + numUser;
        contador++;
    } while (numUser !== 0);

    total = total / (contador - 1);
    console.log(total);

}
//mediaNums();

function maxYMin() {
    var numUser = 0;
    var numMax = 0;
    var numMin = 1000;

    do {
        numUser = prompt("Introduce un numero maxYMin") * 1;
        if (numUser > numMax) {
            numMax = numUser;
        } else if (numUser < numMin && numUser !== 0) {
            numMin = numUser;
        } else {
            numMin = numMin;
        }
    } while (numUser !== 0);

    console.log("Numero maximo:" + numMax + " Numero minimo:" + numMin);

}
//maxYMin();

function fechaHoy() {
    var hoy = new Date();
    var anio = hoy.getFullYear();/*Cogemos el año actual*/
    var mes = hoy.getMonth();/*Cogemos el mes actual*/
    var dia = hoy.getDate();/*Cogemos el dia actual en numero*/

    var dias = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"];
    var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

    console.log(`Hoy es ${dias[dia]} de ${meses[mes]} de ${anio}`);//Aprender bien esta formula
    //console.log("Dia "+dia+ " de "+mes+ " del año "+anio);
}
//fechaHoy();


var f1 = new Date(2019, 0, 1);
var f2 = new Date(2007, 0, 12);

//diasPasados(f1, f2);

function diasPasados(fecha1, fecha2) {

    var fecha1 = fecha1.getTime();
    var fecha2 = fecha2.getTime();
    var diferenciaDeDias = fecha1 - fecha2;

    var diferenciaDeDias = diferenciaDeDias / 1000 / 60 / 60 / 24;

    console.log("Han pasado " + diferenciaDeDias + " dias.");
}

/*
    NUMEROS
*/
/*Mayor*/
function mayor(num1, num2) {

    if (num1 > num2) {
        return num1;
    } else {
        return num2;
    }

}
//probamos la función
/*varDePrueba = mayor(23,4);
console.log(varDePrueba);*/

/*Datos*/
function datos(x) {
    /*
    1240 es par
    NO es divisible por 3
    SI es divisible por 5
    NO es divisible por 7
    */
    var par = "impar";
    var divisibleTres = "NO";
    var divisibleCinco = "NO";
    var divisibleSiete = "NO";

    if (x % 2 == 0) {
        var par = "par";
    }
    if (x % 3 == 0) {
        var divisibleTres = "SI";
    }
    if (x % 5 == 0) {
        var divisibleCinco = "SI";
    }
    if (x % 7 == 0) {
        var divisibleSiete = "SI";
    }
    resultado = `${x} es ${par} / ${divisibleTres} es divisible por 3 / ${divisibleCinco} es divisible por 5 / ${divisibleSiete} es divisible por 7`;
    return resultado;
}
//probamos la función
//varDePrueba = datos(1240);
//console.log(varDePrueba);

/*SumaValores*/
function sumaValores(arr) {
    arrParaSumar = arr;
    total = 0;
    for (var i = 0; i < arrParaSumar.length; i++) {
        total = total + arrParaSumar[i];
    }

    return total;
}
//probamos la función
//varDePrueba = sumaValores([1,3,9,2,3]);
//console.log(varDePrueba);

/*factorial*/
function factorial(x) {
    numFactorial = x;

    var total = 1;
    for (var i = 1; i <= numFactorial; i++) {
        total = total * i;
    }
    return total;
}
//probamos la función
//varDePrueba = factorial(10);
//console.log(varDePrueba);

/*Primo*/
function primo(x) {
    /*Un número es primo si
    sólo es divisible por 1 y
    por sí mismo*/
    var numero = x;
    var resultado = true;//es primo
    for (var i = 2; i < numero; i++) {
        if (numero % i === 0) {
            return resultado = false;//no es primo
        }
    }
    return resultado;
}
//probamos la función
//varDePrueba = primo(17);
//console.log(varDePrueba);

/*fibonacci*/
function fibonacci(x) {
    /*En la serie de Fibonacci
    cada número es la suma
    de los dos anteriores.*/
    var numero = x;
    arrayNumeros = [1, 1];
    for (var i = 2; i < numero; i++) {
        
        arrayNumeros[i] = arrayNumeros[ i - 1 ] + arrayNumeros[ i - 2 ];
        
    }

    return arrayNumeros;
}
//probamos la función
//varDePrueba = fibonacci(10);
//console.log(varDePrueba);


/*primoCifras*/
function primoCifras(x) {
    /*Devuelve el primer
    numero primo de x cifras
    10.007
    introducimos 5*/
    var numero = x;
    var primerNumDeEsasCifras = 1;
    for(var i = 0; i < numero-1; i++){
        primerNumDeEsasCifras = primerNumDeEsasCifras * 10;
    }
    esPrimo = false;
    while(esPrimo == false){
        primerNumDeEsasCifras++;
        esPrimo = primo(primerNumDeEsasCifras);
    }

   
    return primerNumDeEsasCifras;
}
//probamos la función
//varDePrueba = primoCifras(5);
//console.log(varDePrueba);


/*
STRINGS TEXTOS
*/
/*Capitaliza*/
function capitaliza(x) {
    stringCompleto = x;
    //Separamos las palabras por el espacio y las guardamos en un array
    var arrayDeCiudades = stringCompleto.split(" ");

    for (var i = 0; i < arrayDeCiudades.length; i++) {
        arrayDeCiudades[i] = arrayDeCiudades[i].toLowerCase();
        //Con charAt(0) seleccionamos la primera letra y la ponemos mayuscula
        //Con substring(1) empezamos la palabra desde la letra con posicion 1
        arrayDeCiudades[i] = arrayDeCiudades[i].charAt(0).toUpperCase() + arrayDeCiudades[i].substring(1);
    }

    return arrayDeCiudades.join(" ");
}
//probamos la función
varDePrueba = capitaliza("barcelona PARIS");
console.log(varDePrueba);

/*Palabra*/
function palabra(x) {
    var ciudad = x;
    var vocales = ["a", "e", "i", "o", "u"];
    var consonante = 0;
    var vocal = 0;
    var parImpar = "impar"

    for (var i = 0; i < ciudad.length; i++) {
        for (var x = 0; x < vocales.length; x++) {
            if (ciudad.charAt(i) == vocales[x]) {
                vocal = vocal + 1;
            }
        }
    }

    consonante = ciudad.length - vocal;

    if (ciudad.length % 2 == 0) {
        var parImpar = "par";
    }

    resultado = `"Barcelona" tiene ${ciudad.length} letras \n
    ${ciudad.length} es un numero ${parImpar} \n
    Vocales: ${vocal} \n 
    Consonantes: ${consonante}`;

    return resultado;
}
//probamos la función
//varDePrueba = palabra("barcelona");
//console.log(varDePrueba);

/*
FECHAS
*/
/*Hoy*/
function hoy() {
    var fecha = new Date();
    var dias = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"];

    return dias[fecha.getDay()];
}
//probamos la función
//varDePrueba = hoy();
//console.log(varDePrueba);


/*navidad*/
function navidad() {

    var f1 = new Date(2019, 11, 25);
    var f2 = new Date();
    var fecha1 = f1.getTime();
    var fecha2 = f2.getTime();
    var diferenciaDeDias = fecha1 - fecha2;

    diferenciaDeDias = diferenciaDeDias / 1000 / 60 / 60 / 24;

    return "Faltan " + Math.round(diferenciaDeDias); + " dias.";

}
//probamos la función
//varDePrueba = navidad();
//console.log(varDePrueba);


/*analiza*/
function analiza(arr) {

    var arrayConNums = arr;
    var sumaArray = 0;
    var numGrande = 0;
    var numPeque = 0;
    var delimitadorGrande = arrayConNums[0];
    var delimitadorPeque = arrayConNums[0];

    for(var i = 0; i < arrayConNums.length; i++){

        sumaArray = sumaArray + arrayConNums[i];
        if(arrayConNums[i] > delimitadorGrande){
            numGrande = arrayConNums[i];
        }
        if(arrayConNums[i] < delimitadorPeque){
            numPeque = arrayConNums[i];
        }
    }

    return `La suma de los ${arrayConNums.length} es ${sumaArray} \n
    El número mayor es ${numGrande} \n
    El número menos es ${numPeque}`;

}
//probamos la función
//varDePrueba = analiza([2,4,8,-2]);
//console.log(varDePrueba);