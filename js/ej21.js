
/*$(document).on("click"), ("input[id=btnCargarDatos]"), function(){
    alert(123);
}*/

$(document).on("click", "#btnCargarDatos", function () {
    $.getJSON(
        "https://api.citybik.es/v2/networks/bicing",
        function (data) {
            //Cogemos el dato del array que queremos, en este caso las estaciones
            var estacions = data.network.stations;
            //Cogemos el dato del input con el numero
            var numMinDeBicis = $("#numMinDeBicis").val();
            //Lo convertimos a numerico
            numMinDeBicis = parseInt(numMinDeBicis);

            /*Ponemos el body en blanco para que al entrar lo pinte con los 
            datos nuevos*/
            $("table.tablaBicing tbody").html("");

            for (i = 0; i < estacions.length; i++) {
                //Almacenamos los datos que queremos
                var estacion = data.network.stations[i].name;
                var bicisDisponibles = data.network.stations[i].free_bikes;
                var slotsLibres = data.network.stations[i].empty_slots;
                var latitud = data.network.stations[i].latitude;
                var longitud = data.network.stations[i].longitude;

                //Montamos el resto de la tabla con lo datos guardados
                if (numMinDeBicis <= bicisDisponibles) {
                    fila = $("<tr>");
                    col1 = $("<td>").text(estacion);
                    col2 = $("<td>").text(bicisDisponibles);
                    col3 = $("<td>").text(slotsLibres);
                    col4 = $("<td>").text(latitud);
                    col5 = $("<td>").text(longitud);
                    fila.append(col1, col2, col3, col4, col5);

                    $("table.tablaBicing tbody").append(fila);
                }
                //console.log(nombre);
            }
        }
    );
});