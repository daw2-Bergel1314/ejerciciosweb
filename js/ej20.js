/*Ejercicio1*/
$(document).on("click", "body.ej20 div.container div.ej1 div", function () {
    $(this).toggleClass("circuloAmarillo");
});
/*Ejercicio1b*/
$(document).on("click", "input[name=pintarRojo]", function () {
    $("body.ej20 div.container div.ej1b div").addClass("circuloRojo");
});

$(document).on("click", "input[name=pintarGris]", function () {
    $("body.ej20 div.container div.ej1b div").removeClass("circuloRojo");
});
/*Ejercicio2*/
$(document).on("click", "input[name=buttonCopiar]", function () {
    var value = $("input[name=textoACopiar]").val();
    $("input[name=textoAFijar]").val(value);
});
/*Ejercicio3 y 3b*/
/*3*/
$(document).on("click", "input[name=buttonCopiarTabla]", function () {
    var valueACopiar = $("input[name=txtEnTabla").val();
    $("table[name=tableEj3] tbody tr td span").html(valueACopiar);
});
/*3b*/
$(document).on("click", "input[name=buttonConcatenarTabla]", function () {
    var valueACopiar1 = $("table[name=tableEj3] tbody tr td span").text();
    var valueAConcatenar = $("input[name=txtEnTabla").val();
    $("table[name=tableEj3] tbody tr td span").append(valueAConcatenar);
});
/*Ejercicio4 y 4b*/
$(document).on("click", "input[id=btnMenos]", function () {
    var numero = $("input[id=numIntroducido]").val();

    if (numero > 0) {
        numero--;
        $("input[id=numIntroducido]").val(numero);
        //console.log(numero);
    }
});
$(document).on("click", "input[id=btnMas]", function () {
    var numero = $("input[id=numIntroducido]").val();

    if (numero < 10) {
        numero++;
        $("input[id=numIntroducido]").val(numero);

        //console.log(numero);
    }
});
/*Ejercicio5*/
/*Previus*/
$(document).on("click", "a[aria-label=Previous]", function () {

    var primerNumPaginationString = $("a[id=left]").text();
    var primerNumPagination = parseInt(primerNumPaginationString);
    primerNumPagination = primerNumPagination - 1;

    var numero = $("a[id=center]").text();
    var num = parseInt(numero);

    if (primerNumPagination > 0) {
        $("a[id=left]").text(num - 2);//2
        $("a[id=center]").text(num - 1);//3
        $("a[id=right]").text(num);//4

        $("a[id=left]").attr("href", "noticias/noti" + (num - 2) + ".html");
        $("a[id=center]").attr("href", "noticias/noti" + (num - 1) + ".html");
        $("a[id=right]").attr("href", "noticias/noti" + (num) + ".html");
    }
    console.log(primerNumPagination);
});
/*Next*/
$(document).on("click", "a[aria-label=Next]", function () {
    var primerNumPaginationString = $("a[id=right]").text();
    var primerNumPagination = parseInt(primerNumPaginationString);
    primerNumPagination = primerNumPagination;

    var numero = $("a[id=center]").text();
    var num = parseInt(numero);

    if (primerNumPagination < 20) {
        $("a[id=left]").text(num);//4
        $("a[id=center]").text(num + 1);//5
        $("a[id=right]").text(num + 2);//6

        $("a[id=left]").attr("href", "noticias/noti" + (num) + ".html");
        $("a[id=center]").attr("href", "noticias/noti" + (num + 1) + ".html");
        $("a[id=right]").attr("href", "noticias/noti" + (num + 2) + ".html");
    }

    console.log(primerNumPagination);
});
/*Al clicar sobre un número aparezca Alert indicando de qué número se trata*/
$(document).on("click", "a.selector", function () {
    var numClicado = $(this).text();
    alert("Página de noticias número: " + numClicado);
});



/*Ejercicio6 TrioBolitas*/
$(document).on("click", "input[id=generarRandom]", function () {
    for (var i = 1; i <= 3; i++) {
        var valor = Math.floor(Math.random() * 50);
        $("body.ej20 div.ej6 div.circuloRosa p.cir" + i).text(valor);
    }
});


/*Ejercicio7 PiedraPapelTijera SEGUIR*/
$(document).on("click", "input[data-btn=jugada]", function () {
    var jugador1 = $(this).val();


    var arrJugadas = ["piedra", "papel", "tijera"];

    var numRandom = Math.floor(Math.random() * 3);
    var jugador2 = arrJugadas[numRandom];
    console.log(jugador1);
/*
    switch (jugador1) {
        case Piedra:
            alert(123);
            break;
        case papel:
            // code block
            break;
        default:
        // code block
    }
*/
});

/*
$("table[name=tableEj3] tbody tr td span").append(valueAConcatenar);
append = para concatenar caracteres a otro string.

var valueACopiar1 = $("table[name=tableEj3] tbody tr td span").text();
text para coger el value de un campo de texto p o span

$("table[name=tableEj3] tbody tr td span").html(valueACopiar);
preguntar es como text pero para el value? que diferencia hay

var valueACopiar = $("input[name=txtEnTabla").val();
Para coger el valor de un campo

*/