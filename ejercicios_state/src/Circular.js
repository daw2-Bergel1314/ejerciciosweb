
import React from 'react';

/*
export default () => <i style={{fontSize: "100px"}} class="far fa-thumbs-down" />;
*/

/*
Al pulsar sobre el mismo, el color debe cambiar a rojo, verde, azul alternativamente. 
Después de la secuencia, volver a gris.
El color debe guardarse en un state.
*/
class Circular extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            colorFondo: 0
        }
        this.cambiarColor = this.cambiarColor.bind(this);
    }

    cambiarColor(event){
        this.setState({
            colorFondo: (this.state.colorFondo === 3 ? this.state.colorFondo = 1 : this.state.colorFondo + 1)
        })
    }

    render() { 
        let colorAPoner = "";
        if(this.state.colorFondo === 0){
            colorAPoner = "grey";
        }else if(this.state.colorFondo === 1){
            colorAPoner = "red";
        }else if(this.state.colorFondo === 2){
            colorAPoner = "green";
        }else{
            colorAPoner = "blue";
        }
        return (
            <div onClick={this.cambiarColor} colorFondo={this.state.colorFondo} style={{height: this.props.alto, width: this.props.alto, backgroundColor:colorAPoner, borderRadius:"25px"}}>
                 
            </div>
        );
    }
}
 
export default Circular;
