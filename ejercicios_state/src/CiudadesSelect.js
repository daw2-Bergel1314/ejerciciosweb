import React, { Component } from 'react';

class ciudadesSelect extends Component {
    constructor(props) {
        super(props);

        let comarcasUnicas;
        let comarcasUnicasArray;
        let comarcasUnicasOption;
        //Separamos las comarcas repetidas con set y filtramos el objeto con map para seleccionar unicamente las comarcas
        // de el objeto
        comarcasUnicas = new Set(this.props.datos.map(
            ciudad => ciudad.comarca
        ));

        comarcasUnicasArray = Array.from(comarcasUnicas);

        comarcasUnicasOption = comarcasUnicasArray.map(
            comarca => <option key={comarca} value={comarca}>{comarca}</option>
        );

        //console.log(comarcasUnicasArray);
        //console.log(comarcasUnicasOption);

        this.state = {
            comarca: comarcasUnicasOption
        }
        this.cogerValue = this.cogerValue.bind(this);
    }

    cogerValue(evento) {
        // let nuevaComarca = evento.target.value;
        this.setState({ comarcaActual: evento.target.value });
    }
    /*
    import {CIUTATS_CAT} from './Datos';
    <Combo campoPrincipal="comarca" campoSecundario="municipi" datos={CIUTATS_CAT} />
    
    Similar al anterior, pero partimos de dos input SELECT. 
    En el primero se mostrarán las distintas comarcas y el 
    segundo deberá rellenarse con los nombres de las ciudades de cada comarca. 
    Al seleccionar el segundo se mostrará en un <h3>.
    
    Pistas: utilizad evento onChange en el primer SELECT,
     capturar el valor seleccionado con .value y aplicarlo como
      filtro a los objetos que se deben mostrar en el segundo SELECT
    
    */
    render() {

        let opcionsMunicipis = this.props.datos
            .filter(elemento => elemento.comarca === this.state.comarcaActual)
            .map(elemento => elemento.municipi)
            .map(el => <option key={el} value={el}>{el}</option>);

        return (
            <div>
                <select onChange={this.cogerValue}>
                    {this.state.comarca}
                </select>
                <br/>
                <select onChange={this.cogerValue}>
                    {opcionsMunicipis}
                </select>
                <h1>{this.state.comarcaActual}</h1>
            </div>
        );
    }
}

export default ciudadesSelect;