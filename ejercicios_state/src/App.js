import React from "react";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Container, Row, Col, Nav, NavItem, NavLink } from 'reactstrap';

import Manita from './Manita';
import Circular from './Circular';
import Fotos from './Fotos';

import CiudadesSelect from './CiudadesSelect';
import { CIUTATS_CAT_20K } from './datos';

import CardBootstrap from './CardBootstrap.jsx';
import Detalle from './Detalle.jsx';

export default () => (

  <BrowserRouter>
    <Container>
      <Row>
        <Col>
          <Nav>
            <NavItem>
              <Link className="nav-link" to="/Manita">Manita</Link>
            </NavItem>
            <NavItem>
              <Link className="nav-link" to="/Circular">Circular</Link>
            </NavItem>
            <NavItem>
              <Link className="nav-link" to="/Fotos">Fotos</Link>
            </NavItem>
            <NavItem>
              <Link className="nav-link" to="/CiudadesSelect">CiudadesSelect</Link>
            </NavItem>
            <NavItem>
              <Link className="nav-link" to="/CardBootstrap">CardBootstrap</Link>
            </NavItem>            
          </Nav>
        </Col>
      </Row>

      <Switch>
        <Route path="/Manita" component={Manita} />
        <Route path="/Circular" render={() => <Circular alto="50px" />} />
        <Route path="/Fotos" component={Fotos} />
        <Route path="/CiudadesSelect" render={() => <CiudadesSelect datos={CIUTATS_CAT_20K} />} />
        <Route path="/CardBootstrap" component={CardBootstrap} />
        <Route path="/detalle/:idPersona" component={Detalle}  />
      </Switch>

    </Container>
  </BrowserRouter>
);
