
import React from 'react';

/*
export default () => <i style={{fontSize: "100px"}} class="far fa-thumbs-down" />;
*/
class Manita extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            estado: true
        }
        
        this.cambiarEstado = this.cambiarEstado.bind(this);
    }

    cambiarEstado(event){
        this.setState({
            estado: !this.state.estado
        })
    }

    render() { 
        let miClase = "";
        if(this.state.estado){
            miClase = "far fa-thumbs-down";
        }else{
            miClase =  "far fa-thumbs-up";
        }
        return (
            <div onClick={this.cambiarEstado}>
                <i style={{fontSize: "100px"}} className={miClase} onClick={this.cambiarEstado}/>
            </div>
        );
    }
}
 
export default Manita;
