import React from 'react';
import './css/selectEquipos.css';

import fiore from './img/fiore.png';
import everton from './img/everton.png';
import milan from './img/milan.png';
import psg from './img/psg.png';

/*
export default () => <i style={{fontSize: "100px"}} class="far fa-thumbs-down" />;
*/
class Fotos extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = { 
            imgSeleccionada: "fiore"
        }
        this.cambiaImg = this.cambiaImg.bind(this);
    }

    cambiaImg(event){
        this.setState({
            imgSeleccionada: event.target.value
        })
    }


    render() { 

        let imgAMostrar;
        
        if(this.state.imgSeleccionada === "fiore"){
            imgAMostrar = <img src={fiore}/>;
        }else if(this.state.imgSeleccionada === "everton"){
            imgAMostrar = <img src={everton}/>;
        }else if(this.state.imgSeleccionada === "milan"){
            imgAMostrar = <img src={milan}/>;
        }else{
            imgAMostrar = <img src={psg}/>;
        }
        

        return (
            <div className="selectEquipo">
                <select value={this.state.value} onChange={this.cambiaImg}>
                    <option value="fiore">Fiore</option>
                    <option value="everton">Everton</option>
                    <option value="milan">Milan</option>
                    <option value="psg">Psg</option>
                </select>                
                <p>-{imgAMostrar}-</p>

            </div>
        );
    }
}
 
 
export default Fotos;
