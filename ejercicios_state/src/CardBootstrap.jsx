
import React from 'react';

import { Link } from "react-router-dom";
import { Row, Col, Card, CardImg, CardBody, CardTitle, CardSubtitle, CardText, Button } from 'reactstrap';

class CardBootstrap extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            results: []
        }
        this.obtenerDatosPersona = this.obtenerDatosPersona.bind(this);
        this.obtenerDatosPersona();
    }

    obtenerDatosPersona() {
        const apiUrl = `https://randomuser.me/api/?results=10`;

        fetch(apiUrl)
            .then(response => response.json())
            .then(data => this.setState(data))
            .catch(error => console.log(error));
    }

    render() {

        if (!this.state.results.length) {
            return <h1>Cargando datos...</h1>
        }
/*
        let prueba = this.state.results.map(function (usuarios) {
            return usuarios.name.first;
        });
        console.log(prueba);
*/
        let prueba2 = this.state.results.map(function (usuarios) {

            var codUser =
                <Col xs="6" md="4" lg="3" className="border border-dark mt-3" key={usuarios.name.first}>
                    <Card>
                        <CardImg top width="100%" src={usuarios.picture.large} alt="Img user" />
                        <CardBody>
                            <CardTitle className="text-danger font-weight-bolder">
                                {usuarios.name.first} {usuarios.name.last}
                            </CardTitle>
                            <CardSubtitle><small>{usuarios.email}</small></CardSubtitle>
                            <CardText>
                                Location: <br />
                                {usuarios.location.street}<br />
                                {usuarios.location.city}<br />
                                {usuarios.location.state}<br />
                                {usuarios.location.postcode}
                            </CardText>
                            <Link to={"/detalle/"+usuarios.name.first}><Button>OK</Button></Link>
                        </CardBody>
                    </Card>
                </Col>;

            return codUser;
        });
        //console.log(prueba2);

        return (
            <Row>
                {prueba2}                
            </Row>
        );
    }
}

export default CardBootstrap;
