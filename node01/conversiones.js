//node convierte 20 km en m
exports.km_m = (valor) => valor * 1000;
//node convierte 20 m en km
//divide el valor de longitud entre 1000
exports.m_km = (valor) => valor / 1000;


////node convierte 20 km en mi
//divide el valor de velocidad entre 1,609
exports.km_mi = (valor) => valor / 1.609;
//node convierte 20 mi en km
//multiplica el valor de longitud por 1,609
exports.mi_km = (valor) => valor * 1000;


////node convierte 20 m en mi
//valor de longitud entre 1609,344
exports.m_mi = (valor) => valor / 1609.344;
////node convierte 20 mi en m
//valor de longitud por 1609,344
exports.mi_m = (valor) => valor * 1609.344;


//node convierte 20 km/h en m/s
/*divide el valor de velocidad entre 3,6*/
exports.kmh_ms = (valor) => valor / 3.6;
//node convierte 20 m/s en km/h
/*multiplica el valor de velocidad entre 3,6*/
exports.ms_kmh = (valor) => valor * 3.6;


//node convierte 120 km/h en mi/h
//para obtener un resultado aproximado, divide el valor de velocidad entre 1,609
exports.kmh_mih = (valor) => valor / 1.609;
//node convierte 20 mi/h en km/h
/*multiplica el valor de velocidad entre 1.609*/
exports.mih_kmh = (valor) => valor * 1.609;


//node convierte 90 mi/h en m/s
//divide el valor de velocidad entre 2,237
exports.mih_ms = (valor) => valor / 2.237;
//node convierte 90 m/s en mi/h
//multiplica el valor de velocidad entre 2,237
exports.ms_mih = (valor) => valor * 2.237;