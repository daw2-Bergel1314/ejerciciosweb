const geom = require('./convierte');

/* 
La aplicación se debe utilizar del siguiente modo (ejemplos):
node convierte 20 km en mi
> 20 km son XX millas
node convierte 20 km/h en m/s
> 20 km/h son XX m/s
node convierte 120 km/h en mi/h
> 120 km/h son  XX mi/h
node convierte 90 mi/h en m/s
> 90 mi/h son XX m/s
En caso de que nos pidan una conversión incorrecta debe indicarlo:
node convierte 20 km en km/h
> No puedo convertir km en km/h
*/



/*
Este es el ejercicio de geometria

const geom = require('./geometria');

console.log(process.argv);

const figura = process.argv[2];
const valor = process.argv[3];

if (figura=='cuadrado'){
    console.log(`Area de cuadrado de ${valor} de lado: ${geom.areaCuadrado(valor*1)}`);
} else if (figura=='circulo'){
    console.log(`Area de circulo de ${valor} de radio: ${geom.areaCirculo(valor*1)}`);
} else {
    console.log("Error...")
}
*/