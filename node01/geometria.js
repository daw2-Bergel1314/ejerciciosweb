exports.areaCuadrado = (lado) => lado*lado;
exports.areaCirculo = (radio) => radio*radio*Math.PI;

exports.MIL = 1000;