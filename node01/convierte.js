//Vinculamos el documento que contiene las funciones
const conversiones = require('./conversiones');

//ejemplo de ejecucion: node convierte 20 km en m
const valor = process.argv[2]; //Cogemos el dato en posicion 2
const desde = process.argv[3]; //Cogemos el dato en posicion 3
const hacia = process.argv[5]; //Cogemos el dato en posicion 5

//node convierte 20 km en m
if(desde === 'km' && hacia === 'm'){
    console.log(valor+' km son '+ conversiones.km_m(valor)+ " m.");
    return;
}else if (desde === 'm' && hacia === 'km'){//node convierte 20 m en km
    console.log(valor+' m son '+ conversiones.m_km(valor)+ " km.")
    return;    

}else if(desde === 'km' && hacia === 'mi'){//node convierte 20 km en mi
    console.log(valor+' km son '+ conversiones.km_mi(valor)+ " mi.")
    return;    

}else if(desde === 'mi' && hacia === 'km'){//node convierte 20 mi en km
    console.log(valor+' mi son '+ conversiones.mi_km(valor)+ " km.")
    return;    

}else if(desde === 'm' && hacia === 'mi'){//node convierte 20 m en mi
    console.log(valor+' m son '+ conversiones.m_mi(valor)+ " mi.")
    return;    

}else if(desde === 'mi' && hacia === 'm'){//node convierte 20 mi en m
    console.log(valor+' mi son '+ conversiones.mi_m(valor)+ " m.")
    return;    

}else if(desde === 'km/h' && hacia === 'mi/h'){////node convierte 120 km/h en mi/h
    console.log(valor+' km/h son '+ conversiones.kmh_mih(valor)+ " mi/h.");
    return;

}else if(desde === 'mi/h' && hacia === 'km/h'){////node convierte 120 km/h en mi/h
    console.log(valor+' mi/h son '+ conversiones.mih_kmh(valor)+ " km/h.");
    return;

}else if(desde === 'km/h' && hacia === 'm/s'){//node convierte 20 km/h en m/s
    console.log(valor+' km/h son '+ conversiones.kmh_ms(valor)+ " m/s.")
    return;

}else if(desde === 'm/s' && hacia === 'km/h'){//node convierte 20 m/s en km/h 
    console.log(valor+' m/s son '+ conversiones.ms_kmh(valor)+ " km/h.")
    return;

}else if(desde === 'mi/h' && hacia === 'm/s'){//node convierte 90 mi/h en m/s
    console.log(valor+' mi/h son '+ conversiones.mih_ms(valor)+ " m/s.");
    return;
}else if(desde === 'm/s' && hacia === 'mi/h'){//node convierte 90  m/s en mi/h
    console.log(valor+' m/s son '+ conversiones.ms_mih(valor)+ " mi/h.");
    return;
}
//Si no entra en ningun if devilvemos un mensaje de error
console.log('No coincide con ningun criterio de conversion');