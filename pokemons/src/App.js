import React from 'react';

import { BrowserRouter, Switch, Route } from "react-router-dom";
import { Container } from 'reactstrap';

import NavMenu from './components/NavMenu';
import ListaPokemons from './components/ListaPokemons';
import DetallPokemon from './components/DetallPokemon';


/*Components*/
//import HomeComponent from './components/HomeComponent';
//import CursosComponent from './components/CursosComponent';

// importamos css
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';

function App() {
  return (
    <BrowserRouter>
      <NavMenu />
      <Container>
        <Switch>
          <Route exact path="/pokemons" render={() => <ListaPokemons />} />
          <Route exact path="/pokemons/:id" render={(props) => <DetallPokemon  {...props} />} />
        </Switch>
      </Container>
    </BrowserRouter>
  );
}

export default App;
