
import React from 'react';
import { Table } from 'reactstrap';
import { Link } from 'react-router-dom';


const  IconoPokemon = (props) => <img src={"https://img.pokemondb.net/sprites/omega-ruby-alpha-sapphire/dex/normal/"+props.nombre.toLowerCase()+".png"} />;

class ListaPokemons extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            pokemons: [],
        }
        this.getData = this.getData.bind(this);
        this.getData();
    }


    /*
    Conectamos a una api
    y actualizamos el state guardando lo que nos devuelve esta api, 
    que es un array con objetos
    */
    getData() {
        const fetchURL = "http://localhost:3000/api/pokemons";
        fetch(fetchURL)
            .then(results => results.json())
            .then(data => this.setState({ pokemons: data }))
            .catch(err => console.log(err));
    }



    render() {
        /*Hasta que no llegan los resultados no carga la pagina*/
        if (this.state.pokemons.length === 0) {
            return <>...</>;
        }


        //creem les files a partir de dades rebudes, ordenades per id
        let filas = this.state.pokemons.map(item => {
            return (
                <tr key={item.id}>
                    <td >{item.id}</td>
                    <td ><IconoPokemon nombre={item.nombre} /></td>
                    <td>{item.nombre}</td>
                    <td>{item.caracter}</td>
                    <td><Link to={`/pokemons/${item.id}`} className="btn btn-primary">Detall</Link></td>
                </tr>
            );
        })        

        //console.log(this.state.pokemons[5].nombre);

        return (
            <>
                <h1>Pokemons</h1>
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Ico</th>
                            <th>Nombre</th>
                            <th>Caracter</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {filas}
                    </tbody>
                </Table>
            </>
        );
    }
}

export default ListaPokemons;
