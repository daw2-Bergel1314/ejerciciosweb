import React from 'react';
import './css/Capital.css';

export default function Capital(props){

    let inicial = props.nombre[0].toUpperCase();
    let ciudad = inicial + props.nombre.substring(1).toLowerCase();
   
    return <div className="ciudad"><p>{inicial}</p><p>{ciudad}</p></div>;


}