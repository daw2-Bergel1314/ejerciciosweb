import React, { Component } from 'react';
import './App.css';
import Capital from './componentes/Capital';
import Gato from './componentes/Gato';

class App extends Component {
  render() {
    return (
      <React.Fragment>

        <Capital nombre="barcelona" />
        <Gato ancho="400" alto="200" nombre="Aristogato" />
      </React.Fragment>
    );
  }
}



export default App;
