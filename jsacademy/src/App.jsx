
import React from 'react';

import { BrowserRouter, Switch, Route } from "react-router-dom";
import { Container, Row, Col, Button } from 'reactstrap';

/*Components*/
import HomeComponent from './components/HomeComponent';
import CursosComponent from './components/CursosComponent';
import AlumnosComponent from './components/AlumnosComponent';
import NuevoCursoComponent from './components/NuevoCursoComponent';
import NuevoAlumnoComponent from './components/NuevoAlumnoComponent';
import Header from './components/Header';

import 'bootstrap/dist/css/bootstrap.min.css';
import Alumno from './components/Alumno';

class App extends React.Component {



    constructor(props) {
        super(props);

        let datosCurso = [
            { id: 1, nombre: "Curso 150h JS", especialidad: "JavaScript" },
            { id: 2, nombre: "Curso 200h Java", especialidad: "Java" }
        ];

        const alumnosInicio = [
            new Alumno(1, "nom1", "mail1@jones.com", 28),
            new Alumno(2, "nom2", "mail2@bond.com", 39),
            new Alumno(3, "nom3", "mail3@parker.com", 50),
        ];

        this.state = {
            datosAlumnos: alumnosInicio,
            ultimoAlumnoId: 3,
            datosCurso,
            ultimoCursoId: 2
        }
        this.guardaCurso = this.guardaCurso.bind(this);
        this.guardaAlumno = this.guardaAlumno.bind(this);
        this.saveData = this.saveData.bind(this);
        this.loadData = this.loadData.bind(this);
        this.resetData = this.resetData.bind(this);
    }


    guardaCurso(datos) {
        /*Esta funcion se envia a traves de la route de nuevoCurso*/
        /*console.log(datos.nombreCurso);
        console.log(datos.nombreEspecialidad);
        console.log("--------------------");*/

        //solo si id=0 asignamos nuevo id y actualizamos ultimoContacto
        if (datos.idCurso === 0) {
            //Le añadimos al state +1 para tener diferente ID, hacemos setstate
            datos.idCurso = this.state.ultimoCursoId + 1;
            this.setState({ ultimoCursoId: datos.idCurso });
        }
        // finalmente actualizamos state
        this.state.datosCurso.push({ id: datos.idCurso, nombre: datos.nombreCurso, especialidad: datos.nombreEspecialidad })
        //this.setState({datosCurso: nuevoArray});

    }

    guardaAlumno(datos) {
        /*Esta funcion se envia a traves de la route de nuevoAlumno*/
        //solo si id=0 asignamos nuevo id y actualizamos ultimoContacto
        if (datos.idAlumno === 0) {
            //Le añadimos al state +1 para tener diferente ID, hacemos setstate
            datos.idAlumno = this.state.ultimoAlumnoId + 1;
            this.setState({ ultimoAlumnoId: datos.idAlumno });
        }
        // finalmente actualizamos state
        this.state.datosAlumnos.push({ id: datos.idAlumno, nombre: datos.nombreAlumno, email: datos.emailAlumno, edad: datos.edadAlumno, genero: datos.generoAlumno })
        //this.setState({datosCurso: nuevoArray});

    }

    //extra guardado de datos
    saveData() {
        var jsonData = JSON.stringify(this.state);
        localStorage.setItem("datagenda", jsonData);

    }

    //carga de datos
    loadData() {
        var text = localStorage.getItem("datagenda");
        if (text) {
            var obj = JSON.parse(text);
            this.setState(obj);
        } else {
            this.setState(
                {
                    datosAlumnos: [],
                    ultimoAlumnoId: 0,
                    datosCurso: [],
                    ultimoCursoId: 0
                }
            );
        }
    }
    //localStorageclear para limpiar
    resetData() {
        localStorage.clear();
        //this.forceUpdate();
        this.loadData();
    }

    render() {

        return (
            <BrowserRouter>
                <Container>
                    <Row>
                        <Col>
                            <Header />
                        </Col>
                    </Row>

                    <Col>
                        <Row>
                            <Button color="primary" onClick={this.saveData}>Guardar</Button>
                            <Button color="warning" onClick={this.loadData}>Cargar</Button>
                            <Button color="danger" onClick={this.resetData}>Eliminar</Button>
                        </Row>
                    </Col>

                    <Switch>
                        <Route exact path="/" component={HomeComponent} />
                        <Route path="/Cursos" render={() => <CursosComponent datosCurso={this.state.datosCurso} />} />
                        <Route path="/Alumnos" render={() => <AlumnosComponent datosAlumnos={this.state.datosAlumnos} />} />
                        <Route path="/NuevoCurso" render={() => <NuevoCursoComponent guardaCurso={this.guardaCurso} />} />
                        <Route path="/NuevoAlumno" render={() => <NuevoAlumnoComponent guardaAlumno={this.guardaAlumno} />} />
                    </Switch>

                </Container>
            </BrowserRouter>
        );
    }
}

export default App;
