
import React from 'react';

import { Row, Col, Table } from 'reactstrap';

class CursosComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            results: []
        }
    }



    render() {
        
        var tablaDatosCurso = this.props.datosCurso.map(function (datosCurso) {
            return(
                <tr key={datosCurso.id}>
                    <th scope="row">{datosCurso.id}</th>
                    <td>{datosCurso.nombre}</td>
                    <td>{datosCurso.especialidad}</td>
                </tr>
            );
        });

        return (
            <Row>
                <Col xs="12" className="border border-dark">
                    <Table>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Especialidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            {tablaDatosCurso}
                        </tbody>
                    </Table>
                </Col>
            </Row>
        );
    }
}

export default CursosComponent;
