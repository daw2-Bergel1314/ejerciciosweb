
import React from 'react';

import { Redirect } from 'react-router-dom';
import { Row, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';

class NuevoCursoComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nombre: "",
            especialidad: "",
            rediCurso: false,
            idCurso: 0,
            results: []
        }

        this.cambioInput=this.cambioInput.bind(this);
        this.guardarDatosIntroducidosCurso=this.guardarDatosIntroducidosCurso.bind(this);
    }


    cambioInput(e){

        let variable = e.target.name;
        let valor = e.target.value;

        this.setState({
            [variable]: valor
        });
        

    }

    guardarDatosIntroducidosCurso(event){
        
        event.preventDefault();
        let nombreCurso = this.state.nombre;
   
        let nombreEspecialidad = this.state.especialidad;
        let idCurso = this.state.idCurso;
        this.props.guardaCurso({idCurso, nombreCurso, nombreEspecialidad});
        this.setState({ rediCurso: true });
    }

    render() {
        
        if (this.state.rediCurso === true) {
            return <Redirect to='/Cursos' />
        }

        return (
            <div className="container">
                <Row>
                    <Col xs="12" className="border border-dark">
                        <Form onSubmit={this.guardarDatosIntroducidosCurso}>
                            <FormGroup>
                                <Label for="nombreCurso">Nombre</Label>
                                <Input type="text" onChange={this.cambioInput} name="nombre" value={this.state.nombre} id="nombreCurso" placeholder="Nombre del curso" />
                            </FormGroup>
                            <FormGroup>
                                <Label for="especialidadCurso">Especialidad</Label>
                                <Input type="text" onChange={this.cambioInput} name="especialidad" id="especialidadCurso" value={this.state.especialidad} placeholder="Especialidad del curso" />
                            </FormGroup>
                            <Button color="primary">Enviar</Button>
                        </Form>

                    </Col>
                </Row>
            </div>
        );
    }
}

export default NuevoCursoComponent;
