
import React from 'react';

import { Redirect } from 'react-router-dom';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';

class NuevoAlumnoComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: 2,
            nombre: '',
            email: '',
            edad: '',
            genero: '',
            volver: false
        }
        this.cambioInput = this.cambioInput.bind(this);
        this.submit = this.submit.bind(this);
    }
    //gestión genérica de cambio en campo input
    cambioInput(event) {
        const v = event.target.value;
        const n = event.target.name;

        this.setState({
            [n]: v
        });
        
    }

    submit(e) {
        e.preventDefault();
        let nombreAlumno = this.state.nombre;
        let emailAlumno = this.state.email;
        let edadAlumno = this.state.edad;
        let generoAlumno = this.state.genero;
        let idAlumno = 0;

        //ejecutar
        this.props.guardaAlumno({nombreAlumno, emailAlumno, edadAlumno, generoAlumno, idAlumno});
        this.setState({ volver: true });

        //ejecutar
        {/*
        this.props.guardaAlumno({
            nombreAlumno: this.state.nombre,
            emailAlumno: this.state.email,
            edadAlumno: this.state.edad,
            generoAlumno: this.state.genero,
            idAlumno: 0
        });*/}
        
    }

    render() {
        //si se activa volver redirigimos a lista
        if (this.state.volver === true) {
            return <Redirect to='/Alumnos' />
        }
        return (
            <Row>
                <Col xs="12" className="border border-dark">
                    <Form onSubmit={this.submit}>
                        <FormGroup>
                            <Label for="nombreInput">Nombre</Label>
                            <Input type="text"
                                name="nombre"
                                id="nombreInput"
                                value={this.state.nombre}
                                onChange={this.cambioInput} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="emailInput">E-mail</Label>
                            <Input type="text" name="email" id="emailInput"
                                value={this.state.email}
                                onChange={this.cambioInput} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="edadInput">Edad</Label>
                            <Input type="edad" name="edad" id="edadInput"
                                value={this.state.edad}
                                onChange={this.cambioInput} />
                        </FormGroup>        
                        <FormGroup>
                            <Label for="generoInput">Genero</Label>
                            <Input type="genero" name="genero" id="generoInput"
                                value={this.state.genero}
                                onChange={this.cambioInput} />
                        </FormGroup>                                                

                        <Button type="submit" color="primary">Guardar</Button>

                    </Form>
                </Col>
            </Row>
        );
    }
}

export default NuevoAlumnoComponent;
