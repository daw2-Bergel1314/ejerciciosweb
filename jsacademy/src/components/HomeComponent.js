
import React from 'react';

import { Row, Col } from 'reactstrap';

class HomeComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            results: []
        }
    }

    
    render() {

        return (
            <Row>
                <Col xs="12" className="border border-dark" >
                    <img alt="banner" src="https://as1.ftcdn.net/jpg/01/94/01/00/500_F_194010093_9tC5JNVsiEOlVDs2F5Y6d0paYrdWTdbT.jpg" style={{width: "100%"}}/>
                </Col>
            </Row>
        );
    }
}

export default HomeComponent;
