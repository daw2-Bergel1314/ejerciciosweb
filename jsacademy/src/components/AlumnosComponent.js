
import React from 'react';

import { Row, Col, Table } from 'reactstrap';
import { Link } from "react-router-dom";

class AlumnosComponent extends React.Component {


    render() {

        //para crear las filas hacemos un "map" de los contactos recibidos
        //previamente los ordenamos por id...
        let filas = this.props.datosAlumnos.sort((a, b) => a.id - b.id).map(contacto => {
            return (
                <tr key={contacto.id}>
                    <th>{contacto.id}</th>
                    <td>{contacto.nombre}</td>
                    <td>{contacto.email}</td>
                </tr>
            );
        })


        return (
            <Row>
                <Col xs="12" className="border border-dark">
                    <Table>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filas}
                        </tbody>
                    </Table>
                </Col>
            </Row>
        );
    }
}

export default AlumnosComponent;
