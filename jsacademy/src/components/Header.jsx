import React from "react";

import {  Link } from "react-router-dom";
import {  Nav, NavItem } from 'reactstrap';

function Header() {
  return (
    <Nav>
    <NavItem>
        <Link className="nav-link" to="/">Home</Link>
    </NavItem>
    <NavItem>
        <Link className="nav-link" to="/Cursos">Cursos</Link>
    </NavItem>
    <NavItem>
        <Link className="nav-link" to="/Alumnos">Alumno</Link>
    </NavItem>
    <NavItem>
        <Link className="nav-link" to="/NuevoCurso">Nuevo Curso</Link>
    </NavItem>
    <NavItem>
        <Link className="nav-link" to="/NuevoAlumno">Nuevo Alumno</Link>
    </NavItem>
</Nav>
  );
}

export default Header;
