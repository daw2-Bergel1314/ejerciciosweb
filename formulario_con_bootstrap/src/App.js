import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Todolist from './componentes/Todolist';
import Formulari from './componentes/Formulari';
import Llista from './componentes/Llista';

class App extends Component {
  render() {
    return (
      <div className="App">
        
        <Todolist />
        
      </div>
    );
  }
}

export default App;
