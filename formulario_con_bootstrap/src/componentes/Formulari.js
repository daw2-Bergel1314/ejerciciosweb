import React, { Component } from 'react';

class Formulari extends React.Component {
    constructor(props){
        super(props);
        this.state={
            tasca: ''
        }
        this.enviaForm = this.enviaForm.bind(this);
        this.canviTasca = this.canviTasca.bind(this);
    }
    canviTasca(event){
        this.setState({tasca: event.target.value});
    }
    enviaForm(event) {
        this.props.novaTasca(this.state.tasca);
        this.setState({
            tasca: ''
        })
        event.preventDefault();
    }
    render(){
        let i = 1;
        return(
            <form onSubmit={this.enviaForm}>
                <div className="form-group">
                    <label>Tasca</label>
                    <input type="text" value={this.state.tasca} className="form-control"  onChange={this.canviTasca} />
                </div>
                <button type="submit" className="btn btn-primary">Enviar</button>
            </form>
        );
    }
}

export default Formulari;