import React, { Component } from 'react';

class Llista extends React.Component {
    render() {
        let i = 1;
        let tasques = this.props.tasques.map(todo => <Itemlist key={todo.id} esborra={this.props.esborra} tasca={todo} />)
        return (
            <div>
                {tasques}
            </div>
        );
    }
}

class Itemlist extends React.Component {
    render() {
        return (
            <div className="alert alert-primary" role="alert">
                {this.props.tasca.text}
                <button type="button" className="close"
                    onClick={() => this.props.esborra(this.props.tasca)}>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        );
    }
}

export default Llista;