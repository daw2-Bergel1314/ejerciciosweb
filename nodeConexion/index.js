let express = require('express');

let app = express();

app.use(express.urlencoded());

app.get("/", (request, response) => {
    response.send("Conversiones");
});

app.get("/km", (request, response) => {
    response.send(`
        <form action="/millas" method="post">
            <label>Entra km</label>
            <input name="kms">
            <input type="submit" value="convertir">
        </form>
    `);
});

app.post("/millas", (req, res) => {
    console.log(req.body);
    let kms = req.body.kms;
    let millas = kms*1.6;
    res.send(`
        <h2>${kms} kilometros equivalen a ${millas} millas</h2>
    `);
});

app.listen(3003, ()=>{
    console.log("Web online en 3003");
});